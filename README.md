# Build Status
| Master | Develop |
|:-:|:-:|
| ![build](https://gitlab.com/rocket_launcher/controller/badges/master/build.svg) | ![build](https://gitlab.com/rocket_launcher/controller/badges/develop/build.svg) |

# Overview
This code is part of the [LATIS Project](http://latisproject.org)

# Installation
## Pre-requisites:
To install this on your Arduino 101, make sure you have the following:
- [Arduino IDE](https://www.arduino.cc/en/Main/Software)
- Intel Curie library (this can be downloaded via the Boards Manager menu in the Arduino IDE)

## Actual installation:
- Download the latest stable build from the [master branch](https://gitlab.com/rocket_launcher/controller/raw/master/arduino_code/arduino_code.ino)
    - (unless you want to push the limits of our coding skills, in which case, help yourself to the [develop branch](https://gitlab.com/rocket_launcher/controller/raw/develop/arduino_code/arduino_code.ino) but we can't promise things won't break or otherwise won't work properly)
- Open the `arduino_code.ino` file in the Arduino IDE
- Connect your Arduino 101 and upload the code
- Make sure the hardware is wired up per the ["how to build it" guide](http://latisproject.org/build_instructions/)

# Roadmap for the future
The plan for the future is detailed in our [Roadmap/Build Plan](http://latisproject.org/roadmap/)