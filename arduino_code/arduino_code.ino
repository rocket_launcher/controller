/*
    Launch and Telemterty Integrated System (LATIS) Project Code.
    Copyright (C) 2017  Travis Goodwyn, Dmitriy Plaks, Brooke Roberts, Jonathan Townley

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// import the Curie Bluetooth Low Power (BLE) library
#include <CurieBLE.h>
// import the Real Time Clock (RTC) library (to blink LEDs)
#include <CurieTimerOne.h>

///////////////////////////////////////////////////////////
// user settings
const double pwm_led_duty = 50.0; // duty for blinking LED (between 0.0 and 100.0)
const unsigned int pwm_led_timer = 250000; // time for the LED to stay on (in microseconds)
const int igniter_on_time = 8000; // amount of time (in milliseconds) for the igniter to stay on
const int launch_abort_cooldown_time = 30*1000; // amount of time (in milliseconds) to wait until rocket can be launched again

// don't change things below this line unless you know what you're doing
///////////////////////////////////////////////////////////


//   initialize bluetooth
BLEPeripheral blePeriph;

// set the services and characteristics UUIDs
const char uuid_launch[ ] = "66A8E213-68A4-4699-8B7A-C351EE01668F";
const char uuid_safety[ ] = "FD3B44CE-22B6-4114-9E0D-AC0272396465";

// create the BLE services
BLEService launchService(uuid_launch); 
BLEService safetyService(uuid_safety);

// create the BLE characteristics
BLEUnsignedCharCharacteristic launchCharacteristic(uuid_launch, BLERead | BLEWrite);
BLEUnsignedCharCharacteristic safetyCharacteristic(uuid_safety, BLERead); // change to BLEBoolCharacteristic in the future

// declare other variables
bool launch_aborted = false; // this variable is set to TRUE when the abort button is pressed on the controller (but not the app)
bool launch_aborted_recently = false; // this variable is set to TRUE right after the abort button is pressed on the conroller
unsigned long launch_abort_cooldown_timer = 0; // variable that counts amount of time that passes after the abort button is pressed
const float safety_interlock_voltage_threshold = 1.5;
bool safety_interlock_state = true; // this variable is set to FALSE when rocket is READY to launch

//  declare PINS
const int pin_igniter_relay = 2;  // pin for the actual igniter
const int pin_igniter_led = 5;  // pin for the igniter LED on the controller (RED LED)
const int pin_buzzer = 7;  // pin for the launch warning buzzer on the controller
const int pin_link_led = 3; // pin for BLE connection state (on when something is connected) (BLUE LED)
const int pin_abort_button = 8;  // pin for the hardware abort button 
const int pin_launch_warning_led = 4; // pin for the launch warning LED on the controller (YELLOW LED)
const int pin_safety_interlock = 11; // pin for the safety interlock circuit 
const int pin_safety_interlock_led = 6; // pin for the safety interlock status (YELLOW LED)


///////////////////////////////////////////////////////////
//  other functions

// stop the launch warning
void launch_warning_stop() {
  digitalWrite(pin_igniter_relay, LOW);
  digitalWrite(pin_igniter_led, LOW);
  digitalWrite(pin_buzzer, LOW);
  CurieTimerOne.pwmStop();
}

// launch rocket
void launch_rocket() {
  Serial.println("Rocket was launched!!!");
  digitalWrite(pin_igniter_relay, HIGH);
  digitalWrite(pin_igniter_led, HIGH);
}

// launch warning
void launch_warning_start() {
  digitalWrite(pin_buzzer, HIGH); 
  CurieTimerOne.pwmStart(pin_launch_warning_led, pwm_led_duty, pwm_led_timer);
}

// check safety interlock state
// return value = true: READY to launch
// return value = false: NOT ready to launch
bool check_safety_interlock_state() {
  bool result = digitalRead(pin_safety_interlock);
  // write result to BLE (this will be cleaner when the CurieBLE library is fixed in include Bool)
  if (result) {
    // READY for launch
    safetyCharacteristic.setValue(1);
    digitalWrite(pin_safety_interlock_led, HIGH);
  } else {
    // NOT ready for launch
    safetyCharacteristic.setValue(0);
    digitalWrite(pin_safety_interlock_led, LOW);
  }
  return result;
  
}

// abort launch
void launch_abort() {
  launch_aborted = true;
  launch_aborted_recently = true;
}

void check_recent_aborts() {
  // check for recent launch abort flag & update timer
  if (launch_aborted_recently) {
    // check to see if the cooldown time has been reached (i.e. we can launch again)
    if (millis() > (launch_abort_cooldown_timer + launch_abort_cooldown_time)) {
      launch_aborted_recently = false;
    }
  }
}

// do these things on bluetooth connect
void on_ble_connect() {
  // turn on link led
  digitalWrite(pin_link_led, HIGH);
}

// do these things on bluetooth disconnect
void on_ble_disconnect() {
  // go to launch state 0 on disconnect
  launch_warning_stop();
  launchCharacteristic.setValue(0);
  Serial.println("Launch State = 0");

  // turn off link led
  digitalWrite(pin_link_led, LOW);
}


  
///////////////////////////////////////////////////////////
//  do setup things

void setup() {
  // start Serial
  Serial.begin(9600);
  Serial.println("Starting BLE");
  
  // initialize digital pins
  pinMode(pin_igniter_relay, OUTPUT);
  pinMode(pin_buzzer, OUTPUT);
  pinMode(pin_link_led, OUTPUT);
  pinMode(pin_abort_button, INPUT_PULLUP);
  pinMode(pin_igniter_led, OUTPUT);
  pinMode(pin_safety_interlock, INPUT);
  pinMode(pin_safety_interlock_led, OUTPUT);
  

  // set the local name peripheral advertises
  blePeriph.setLocalName("RocketLauncher");
  
  // connect services and characteristics (note: order matters in this kind of implementation)
  // launch 
  blePeriph.addAttribute(launchService);
  blePeriph.addAttribute(launchCharacteristic);

  // safety
  blePeriph.addAttribute(safetyService);
  blePeriph.addAttribute(safetyCharacteristic);

  // set initial values for the characteristics
  launchCharacteristic.setValue(0);  // state 0 is normal state
  safetyCharacteristic.setValue(0);  // state FALSE (0) is safe
  

  // start advertising
  blePeriph.begin();
  Serial.println("BLE Started");

  // declare interrupt for launch abort
  attachInterrupt(pin_abort_button, launch_abort, RISING);

}



///////////////////////////////////////////////////////////
//  main loop
void loop() {
  // wait for a connection
  BLECentral central = blePeriph.central();

  // if something connects, then do stuff
  if (central) {
    // do the initial things when something connects
    on_ble_connect();

    // print the mac address of the connected device
    Serial.print("connected to: ");
    Serial.println(central.address());
    
    // while connected, do these things
    while (central.connected()) {

      // poll for BLE events
      blePeriph.poll();

      // check interlock status
      safety_interlock_state = check_safety_interlock_state();

      // check to see if the app wrote to the launch characteristic
      if (launchCharacteristic.written()) {
        
        // normal/abort state
        if (launchCharacteristic.value() == 0) {
         // just in case, always stop the launch warning
         launch_warning_stop();
         Serial.println("Launch State = 0");
        }

        // launch warning state
        // only allow to enter this state if launch was NOT aborted recently
        else if (launchCharacteristic.value() == 1 && !launch_aborted_recently && safety_interlock_state) {
          //start launch warning
          launch_warning_start();
          Serial.println("Launch State = 1");
        }

        // launch state
        // only allow to enter this state if launch was NOT aborted recently
        else if (launchCharacteristic.value() == 2 && !launch_aborted_recently && safety_interlock_state) {
          
          // check for launch abort first (just in case - this should not actually ever happen)
          if (launch_aborted) {
            // go to aborted state
            launchCharacteristic.setValue(0);
            launch_warning_stop();
            launch_aborted = false;
            Serial.println("Launch aborted. I can't launch a rocket now!");
          }
          
          else {
            //launch the rocket
            Serial.println("Launch State = 2");
            launch_rocket();
            delay(igniter_on_time);
            launch_warning_stop();
            launchCharacteristic.setValue(0);
            Serial.println("Launch State = 0");
          }
        }

        // recently aborted a launch
        else if (launch_aborted_recently) {
          // go back to launch_state=0
          launchCharacteristic.setValue(0);
          Serial.println("can't transistion states because controller abort button was pressed resently");
        }
        
        // safety interlock is enabled (prevents a launch)
        else if (!safety_interlock_state) {
          Serial.println("can't transition states because safety interlock is enabled");
        }
        
        else {
          // invalid launch characterstic value
          Serial.println("Invalid launchCharacteristic value");
          // value should be reset to a known safe state
          launchCharacteristic.setValue(0);
          Serial.println("Launch State = 0");
        }
      }

      // do other things here that do not require changing of the launch characteristic
      
      // check for launch abort button press (interrupt)
      if (launch_aborted) {
        // reset the flag (so we don't continuously trigger this section)
        launch_aborted = false;
        // go to launch_state=0 
        launchCharacteristic.setValue(0);
        // stop the launch warning
        launch_warning_stop();
        Serial.println("Launch abort button pressed. Transitioning to aborted state");
        // start the abort timer
        launch_abort_cooldown_timer = millis(); 
      }  

      // check for a recent abort command
      check_recent_aborts();
    }


    // do things on disconnect
    on_ble_disconnect();
    
    // send a message on disconnect
    Serial.print("Disconnected from: ");
    Serial.println(central.address());

  }

  // things to do when nothing is connected ////

  // check for launch abort button press (interrupt)
  if (launch_aborted) {
   // reset the flag (so we don't continuously trigger this section)
   launch_aborted = false;
   Serial.println("Launch abort button pressed");
   // start the abort timer
   launch_abort_cooldown_timer = millis(); 
  }

  // check for a recent abort command
  check_recent_aborts();

  // check the safety interlock state
  check_safety_interlock_state();
}

